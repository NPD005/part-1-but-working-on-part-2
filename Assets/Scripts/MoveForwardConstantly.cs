﻿using UnityEngine;
using System.Collections;

public class MoveForwardConstantly : MonoBehaviour
{

    [SerializeField]
    private float ObjectAcceleration = 50f;

    [SerializeField]
    private float ObjectInitialVelocity = 5f;

    private Rigidbody2D ourRigidbody;

    // Use this for initialization
    void Start()
    {
        ourRigidbody = GetComponent<Rigidbody2D>();

        ourRigidbody.velocity = Vector2.up * ObjectInitialVelocity;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 ForceToAdd = Vector2.up * ObjectAcceleration * Time.deltaTime;

        ourRigidbody.AddForce(ForceToAdd);
    }
}